package encoding

import (
	"bytes"
	"github.com/chai2010/webp"
	"image"
)

func Encode(img image.Image) []byte {
	var buf bytes.Buffer

	if err := webp.Encode(&buf, img, &webp.Options{Lossless: false, Quality: 65}); err != nil {
		panic(err.Error())
	}

	return buf.Bytes()
}
