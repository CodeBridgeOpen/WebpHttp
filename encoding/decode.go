package encoding

import (
	"bytes"
	"golang.org/x/image/bmp"
	"golang.org/x/image/tiff"
	"image"
	"image/gif"
	"image/jpeg"
	"image/png"
	"net/http"
	"path/filepath"
)

func Decode(img []byte, name string) (i image.Image) {

	var err error
	switch filepath.Ext(name) {
	case ".jpg":
		fallthrough
	case ".jpeg":
		i, err = jpeg.Decode(bytes.NewReader(img))
	case ".png":
		i, err = png.Decode(bytes.NewReader(img))
	case ".gif":
		i, err = gif.Decode(bytes.NewReader(img))
	case ".bmp":
		i, err = bmp.Decode(bytes.NewReader(img))
	case ".tiff":
		i, err = tiff.Decode(bytes.NewReader(img))
	default:
		panic(http.StatusNotImplemented)
	}

	if err != nil {
		panic(err.Error())
	}

	return i
}

func IsDecodableAsImage(name string) bool {
	ext := filepath.Ext(name)

	types := []string{
		".jpg",
		".jpeg",
		".png",
		".gif",
		".bmp",
		".tiff",
	}

	for _, t := range types {
		if ext == t {
			return true
		}
	}

	return false
}
