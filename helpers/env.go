package helpers

import "os"

func Env(v string, def string) string {
	i := os.Getenv(v)

	if i == "" {
		i = def
	}

	return i
}
