package server

import (
	"io/ioutil"
	"net/http"
	"strconv"
	. "webphttp/store"
	. "webphttp/web"
)

func Handler(w http.ResponseWriter, r *http.Request) {
	defer Recover(w, r)
	Favicon(r.RequestURI)

	defer func() { go CheckCache() }()

	if http.MethodPut == r.Method {
		put(w, r)
		return
	}

	get(w, r)
	return
}

func get(w http.ResponseWriter, r *http.Request) {
	k := r.RequestURI[1:]

	if len(Retrieve(k).Bytes()) == 0 {
		RecordMiss(k)
		panic(http.StatusNotFound)
	}

	RecordHit(k)
	Serve(w, Retrieve(k).Bytes(), Retrieve(k).Metadata())
}

func put(w http.ResponseWriter, r *http.Request) {
	defer r.Body.Close()
	k := r.RequestURI[1:]

	buff, err := ioutil.ReadAll(r.Body)
	if err != nil {
		panic(http.StatusInternalServerError)
	}

	Store(k, buff, GetHeaders(r.Header))
	w.Header().Set("Status", strconv.Itoa(http.StatusAccepted))
}
