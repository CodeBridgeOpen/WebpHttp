package web

import (
	"net/http"
	"strconv"
	"time"
	"webphttp/store"
)

func Serve(w http.ResponseWriter, b []byte, h map[string]string) {
	for key, value := range h {
		if AllowedHeader(key) {
			w.Header().Set(key, value)
		}
	}

	w.Header().Set("Content-Size", strconv.Itoa(len(b)))
	_, _ = w.Write(b)
}

func ServeOriginal(w http.ResponseWriter, itm store.Item) {
	Serve(w, itm.Bytes(), itm.Metadata())
}

func ServeWebp(w http.ResponseWriter, itm store.Item) {
	Serve(w, itm.Bytes(), map[string]string{
		"Pragma":        "public",
		"Content-Type":  "image/webp",
		"Cache-Control": "max-age=290304000",
		"Expires":       time.Now().AddDate(60, 0, 0).Format(http.TimeFormat),
	})
}
