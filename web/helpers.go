package web

import (
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"strings"
	"webphttp/encoding"
)

func Recover(w http.ResponseWriter, r *http.Request) {
	if re := recover(); re != nil {
		if status, ok := re.(int); ok {
			http.Error(w, fmt.Sprintf("%d - %s", status, http.StatusText(status)), status)
			log.Println(fmt.Sprintf("[%d] - %s", re, r.RequestURI))
		} else {
			http.Error(w, fmt.Sprintf(
				"%d - %s",
				http.StatusInternalServerError,
				http.StatusText(http.StatusInternalServerError),
			), http.StatusInternalServerError)
			log.Println(fmt.Sprintf("[%s] - %s", re, r.RequestURI))
		}
	}
}

func Favicon(uri string) {
	if strings.Contains(uri, "favicon.ico") {
		panic(http.StatusNotFound)
	}
}

func WantsWebp(r *http.Request) bool {
	return strings.Contains(r.Header.Get("Accept"), "image/webp")
}

func IsEligibleForWeb(r *http.Request) bool {
	return encoding.IsDecodableAsImage(r.RequestURI)
}

func DownloadImage(url string) (res []byte, meta map[string]string) {
	response, e := http.Get(url)
	if e != nil {
		panic(e.Error())
	}

	if response.StatusCode == http.StatusNotFound {
		return res, meta
	}

	res, _ = ioutil.ReadAll(response.Body)
	response.Body.Close()
	return res, GetHeaders(response.Header)
}

func AllowedHeader(h string) bool {
	switch h {
	case "accept-encoding":
		fallthrough
	case "accept-ranges":
		fallthrough
	case "content-length":
		fallthrough
	case "server":
		fallthrough
	case "x-amz-id-2":
		fallthrough
	case "x-amz-request-id":
		fallthrough
	case "user-agent":
		fallthrough
	case "date":
		return false
	}

	return true
}

func GetHeaders(h http.Header) map[string]string {
	out := map[string]string{}
	for name, headers := range h {
		name = strings.ToLower(name)

		if AllowedHeader(name) {
			val := ""
			for _, h := range headers {
				val = val + h
			}

			out[name] = val
		}
	}

	return out
}
