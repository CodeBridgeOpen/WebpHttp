package worker

import (
	"bytes"
	"net/http"
	. "webphttp/encoding"
	"webphttp/helpers"
	. "webphttp/mappings"
	. "webphttp/store"
	. "webphttp/web"
)

func Handler(o []OriginMap) func(w http.ResponseWriter, r *http.Request) {
	return func(w http.ResponseWriter, r *http.Request) {
		defer Recover(w, r)
		Favicon(r.RequestURI)

		defer func() { go CheckCache() }()

		if WantsWebp(r) && IsEligibleForWeb(r) {
			handle("webp", o, w, r, ServeWebp)
			return
		}

		handle("original", o, w, r, ServeOriginal)
		return
	}
}

func handle(t string, o []OriginMap, w http.ResponseWriter, r *http.Request, s func(w http.ResponseWriter, itm Item)) {
	ref := getOrigin(o, r)
	key := getKey(t, ref, r)
	originalKey := getKey("original", ref, r)

	local := Retrieve(key)
	if len(local.Bytes()) > 0 {
		s(w, Retrieve(key))
		return
	}

	if b, meta := checkServer(key); len(b) > 0 {
		s(w, Store(key, b, meta))
		return
	}

	b, meta := getOriginal(r.RequestURI, ref)

	if t == "webp" {
		Store(originalKey, b, meta)
		go report(originalKey, b, meta)

		img := Decode(b, r.RequestURI)
		b = Encode(img)

		meta["content-type"] = "image/webp"
	}

	go report(key, b, meta)
	s(w, Store(key, b, meta))
	return
}

func report(key string, buff []byte, metadata map[string]string) {
	if server := server(); server != "" {
		req, _ := http.NewRequest(http.MethodPut, server+key, bytes.NewReader(buff))

		for key, value := range metadata {
			if key != "date" {
				req.Header.Set(key, value)
			}
		}

		_, _ = http.DefaultClient.Do(req)
	}
}

func checkServer(k string) (res []byte, meta map[string]string) {
	if server := server(); server != "" {
		return DownloadImage(server + k)
	}

	return res, meta
}

func server() string {
	return helpers.Env("WEBP_HTTP_SERVER_URL", "")
}
