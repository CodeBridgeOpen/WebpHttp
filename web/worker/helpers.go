package worker

import (
	"fmt"
	"net/http"
	. "webphttp/mappings"
	. "webphttp/web"
)

func getKey(t string, originMap OriginMap, r *http.Request) string {
	return fmt.Sprintf("%s+%s+%s", t, originMap.Referer, r.RequestURI)
}

func getOrigin(o []OriginMap, r *http.Request) (m OriginMap) {
	for _, origin := range o {
		if origin.IsHost(r) {
			return origin
		}
	}

	panic(http.StatusUnauthorized)
	return m
}

func getOriginal(uri string, m OriginMap) (res []byte, meta map[string]string) {
	for _, origin := range m.Origins {
		res, meta := DownloadImage(origin + uri)
		if len(res) > 0 {
			return res, meta
		}
	}

	panic(http.StatusNotFound)
	return res, meta
}
