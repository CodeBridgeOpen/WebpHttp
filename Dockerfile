FROM golang:1.11-alpine as builder

RUN apk add git openssl openssh alpine-sdk ca-certificates

WORKDIR /root/.ssh
RUN ssh-keyscan gitlab.com >> known_hosts

WORKDIR /build/gitlab.com/CodeBridgeOpen/WebpHttp
COPY . .
RUN go mod download
RUN go build -o webphttp

FROM alpine:latest
WORKDIR /root/
COPY --from=builder /etc/ssl/certs/ca-certificates.crt /etc/ssl/certs/
COPY --from=builder /build/gitlab.com/CodeBridgeOpen/WebpHttp/webphttp .
EXPOSE 8080
CMD ["./webphttp"]
