package store

import (
	"encoding/json"
	"net/http"
	"strconv"
	"webphttp/helpers"
)

func CacheMap() func(w http.ResponseWriter, r *http.Request) {
	return func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Content-Type", "application/json")

		out := map[string]int{}
		for key, val := range *list {
			out[key] = len(val.Bytes())
		}

		b, _ := json.Marshal(out)

		_, _ = w.Write(b)
	}
}

func MetaMap() func(w http.ResponseWriter, r *http.Request) {
	return func(w http.ResponseWriter, r *http.Request) {
		enableCors(w)
		w.Header().Set("Content-Type", "application/json")

		out := map[string]map[string]string{}
		for key, val := range *list {
			out[key] = val.Metadata()
		}

		b, _ := json.Marshal(out)

		_, _ = w.Write(b)
	}
}

func CacheStatistics() func(w http.ResponseWriter, r *http.Request) {
	return func(w http.ResponseWriter, r *http.Request) {
		enableCors(w)

		w.Header().Set("Content-Type", "application/json")
		b, _ := json.Marshal(s)
		_, _ = w.Write(b)
	}
}

func MemoryUsage() func(w http.ResponseWriter, r *http.Request) {
	return func(w http.ResponseWriter, r *http.Request) {
		enableCors(w)
		w.Header().Set("Content-Type", "application/json")

		max, _ := strconv.ParseFloat(helpers.Env("WEBP_HTTP_MAX_MEMORY", "128.0"), 64)
		o := map[string]interface{}{
			"AvailableMemory": max,
			"UsedMemory":      GetMemoryUsage(),
		}

		b, _ := json.Marshal(o)
		_, _ = w.Write(b)
	}
}

func enableCors(w http.ResponseWriter) {
	w.Header().Set("Access-Control-Allow-Origin", "*")
	w.Header().Set("Access-Control-Allow-Methods", "POST, GET, OPTIONS, PUT, DELETE")
	w.Header().Set("Access-Control-Allow-Headers", "Accept, Content-Type, Content-Length, Accept-Encoding, X-CSRF-Token, Authorization")
}
