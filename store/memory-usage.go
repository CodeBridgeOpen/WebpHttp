package store

var memory_usage = float64(0)

func SetMemoryUsage(usage float64) {
	memory_usage = usage
}

func GetMemoryUsage() float64 {
	return memory_usage
}
