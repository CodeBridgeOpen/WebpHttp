package store

import (
	"strings"
	"sync"
)

var mutex = &sync.Mutex{}
var s = &Statistics{
	General: Stat{
		Hits:   0,
		Misses: 0,
	},
	Origins: map[string]Stat{},
	Types:   map[string]Stat{},
	Files:   map[string]Stat{},
}

type Stat struct {
	Hits   int64
	Misses int64
}

type Statistics struct {
	General Stat
	Origins map[string]Stat
	Types   map[string]Stat
	Files   map[string]Stat
}

type KeyStats struct {
	File   string
	Origin string
	Type   string
}

func RecordHit(k string) {
	mutex.Lock()
	defer mutex.Unlock()

	stats := parse(k)
	s.General.Hits = s.General.Hits + 1

	if st, ok := s.Origins[stats.Origin]; ok {
		st.Hits = st.Hits + 1
		s.Origins[stats.Origin] = st
	} else {
		s.Origins[stats.Origin] = Stat{
			Hits: 1,
		}
	}

	if st, ok := s.Files[stats.File]; ok {
		st.Hits = st.Hits + 1
		s.Files[stats.File] = st
	} else {
		s.Files[stats.File] = Stat{
			Hits: 1,
		}
	}

	if st, ok := s.Types[stats.Type]; ok {
		st.Hits = st.Hits + 1
		s.Types[stats.Type] = st
	} else {
		s.Types[stats.Type] = Stat{
			Hits: 1,
		}
	}
}

func RecordMiss(k string) {
	mutex.Lock()
	defer mutex.Unlock()

	stats := parse(k)
	s.General.Misses = s.General.Misses + 1

	if st, ok := s.Origins[stats.Origin]; ok {
		st.Misses = st.Misses + 1
		s.Origins[stats.Origin] = st
	} else {
		s.Origins[stats.Origin] = Stat{
			Misses: 1,
		}
	}

	if st, ok := s.Files[stats.File]; ok {
		st.Misses = st.Misses + 1
		s.Files[stats.File] = st
	} else {
		s.Files[stats.File] = Stat{
			Misses: 1,
		}
	}

	if st, ok := s.Types[stats.Type]; ok {
		st.Misses = st.Misses + 1
		s.Types[stats.Type] = st
	} else {
		s.Types[stats.Type] = Stat{
			Misses: 1,
		}
	}
}

func parse(k string) KeyStats {
	p := strings.Split(k, "+")
	return KeyStats{p[2], p[1], p[0]}
}
