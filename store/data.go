package store

import (
	"log"
	"math/rand"
	"strconv"
	"webphttp/helpers"
)

var list *map[string]Item

type Item struct {
	buff []byte
	meta map[string]string
}

func (i Item) Bytes() []byte {
	return i.buff
}

func (i Item) Metadata() map[string]string {
	return i.meta
}

func Init() {
	list = &map[string]Item{}
}

func Retrieve(key string) Item {
	return (*list)[key]
}

func Store(key string, b []byte, metadata map[string]string) Item {
	itm := Item{
		b,
		metadata,
	}

	store := helpers.Env("WEBP_HTTP_STORE_LOCALLY", "yes")
	if store != "yes" {
		return itm
	}

	(*list)[key] = itm

	return Retrieve(key)
}

func CheckCache() {
	r := rand.Intn(100)
	if r < 5 {
		log.Println("Checking cache (" + strconv.Itoa(r) + ")")
		size := 0
		l := *list
		for i := range l {
			size = size + len(l[i].buff)
		}

		mb := float64(float64(size)/float64(1024)) / float64(1024)
		SetMemoryUsage(mb)
		if max, _ := strconv.ParseFloat(helpers.Env("WEBP_HTTP_MAX_MEMORY", "128.0"), 64); mb >= max {
			const purgeCount = 20
			log.Println("Purging items from the cache", purgeCount)
			cleanMemory(purgeCount)
		} else {
			log.Println("Cache size is fine", mb)
		}
	}
}

func cleanMemory(count int) {
	deleted := 0
	for i := range *list {
		delete(*list, i)
		deleted = deleted + 1

		if deleted >= count {
			log.Println("Purged items from cache", count)
			return
		}
	}
}
