package main

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"webphttp/helpers"
	. "webphttp/mappings"
	"webphttp/store"
	"webphttp/web/server"
	"webphttp/web/worker"
)

func main() {
	store.Init()

	mode := helpers.Env("WEBP_HTTP_RUN_AS", "worker")

	fmt.Println("Running WebpHttp as " + mode)
	reportEnv()

	switch mode {
	case "worker":
		runWorker()
	case "server":
		runServer()
	}

	if err := serve(); err != nil {
		log.Fatal(err)
	}
}

func runWorker() {
	var origins []OriginMap
	b := []byte(helpers.Env("WEBP_HTTP_ORIGINS", "[]"))
	_ = json.Unmarshal(b, &origins)

	http.HandleFunc("/", worker.Handler(origins))
	http.HandleFunc("/cache-map", store.CacheMap())
	http.HandleFunc("/meta-map", store.MetaMap())
}

func runServer() {
	http.HandleFunc("/", server.Handler)
	http.HandleFunc("/api/cache-map", store.CacheMap())
	http.HandleFunc("/api/meta-map", store.MetaMap())
	http.HandleFunc("/api/statistics", store.CacheStatistics())
	http.HandleFunc("/api/memory-usage", store.MemoryUsage())
}

func reportEnv() {
	fmt.Println("WEBP_HTTP_PORT", helpers.Env("WEBP_HTTP_PORT", ":8080"))
	fmt.Println("WEBP_HTTP_MAX_MEMORY", helpers.Env("WEBP_HTTP_MAX_MEMORY", "128.0"))
	fmt.Println("WEBP_HTTP_ORIGINS", helpers.Env("WEBP_HTTP_ORIGINS", "[]"))
	fmt.Println("WEBP_HTTP_RUN_AS", helpers.Env("WEBP_HTTP_RUN_AS", "worker"))
	fmt.Println("WEBP_HTTP_SERVER_URL", helpers.Env("WEBP_HTTP_SERVER_URL", ""))
	fmt.Println("WEBP_HTTP_STORE_LOCALLY", helpers.Env("WEBP_HTTP_STORE_LOCALLY", "yes"))
}

func serve() error {
	return http.ListenAndServe(helpers.Env("WEBP_HTTP_PORT", ":8080"), nil)
}
