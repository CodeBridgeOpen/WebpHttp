package mappings

import (
	"net/http"
	"strings"
)

type OriginMap struct {
	Referer string
	Origins []string
}

func (o *OriginMap) IsHost(r *http.Request) bool {
	return strings.Contains(r.Host, o.Referer)
}
